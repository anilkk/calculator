(function() {

    var Calculator = {
        /**
         * @method init
         * @description Initial Calculator app
         * @public
         * @param  {object} config 
         */
        init: function(config) {
            Calculator._setdata(config);
            Calculator._cacheDomreference(config);
            Calculator._activateDomEvents();
        },
        /**
         * @method _setdata
         * @description set data from config
         * @private
         * @param  {object} config
         */
        _setdata: function(config) {
            Calculator.numberInputButtonClass = config.numberInputButtonClass;
            Calculator.clearButtonClass = config.clearButtonClass;
            Calculator.resultButtonClass = config.resultButtonClass;
            Calculator.operatorInputButtonClass = config.operatorInputButtonClass;
            Calculator.exponentialToMathConverter = config.exponentialToMathConverter;
        },
        /**
         * @method _cacheDomreference
         * @description  cache DOM reference 
         * @private
         * @param  {object} config
         */
        _cacheDomreference: function(config) {
            Calculator.inputDisplayField = document.querySelector('.' + config.inputDisplayFieldClass);
           Calculator.clearButton = document.querySelector('.' + Calculator.clearButtonClass);
            Calculator.resultButton = document.querySelector('.' + Calculator.resultButtonClass);
        },
        /**
         * @method _activateDomEvents
         * @description  attach DOM events 
         * @private
         */
        _activateDomEvents: function() {
            document.addEventListener('click', Calculator._handleInputClick);
        },
        /**
         * @method _handleInputClick
         * @description  handle calculator buttons click 
         * @private
         * @param  {object} event
         */
        _handleInputClick: function(event) {
        	Calculator._saveEventDomReference(event);
            Calculator._clearInputDisplayErrorMessage();
            if (Calculator._isClickOnResultButton()) {
                Calculator._updateResultDisplayValue(Calculator._evaluateExpression(Calculator._getformattedExpression(Calculator._getDisplayValue())));
            } else if (Calculator._isClickOnNumberFields()) {
                Calculator._updateInputDisplayValue(Calculator.currentButtonTargetDataValue);
            } else if (Calculator._isClickOnClearButton()) {
                Calculator._clearInputDisplay();
            }
        },
        /**
         * @method _saveEventDomReference
         * @description  save reference to event target and className
         * @private
         * @param  {object} event
         */
        _saveEventDomReference: function  (event) {
        	Calculator.currentButtonTarget = event.target;
        	Calculator.currentButtonTargetClass = Calculator.currentButtonTarget.className;	
        	Calculator.currentButtonTargetDataValue = Calculator.currentButtonTarget.getAttribute('data-value');
        },
        _isClickOnNumberFields: function() {
            return ((Calculator.currentButtonTarget.className.indexOf(Calculator._getNumberInputClassname()) > -1) || (Calculator.currentButtonTarget.className.indexOf(Calculator._getOperatorInputClassname()) > -1));
        },
        _isClickOnClearButton: function() {
            return Calculator.currentButtonTarget.className.indexOf(Calculator._getClearButtonClassname()) > -1;
        },
        _isClickOnResultButton: function() {
            return Calculator.currentButtonTarget.className.indexOf(Calculator._getResultButtonClassName()) > -1;
        },
        _getNumberInputClassname: function() {
            return Calculator.numberInputButtonClass;
        },
        _getOperatorInputClassname: function() {
            return Calculator.operatorInputButtonClass;
        },
        _getClearButtonClassname: function() {
            return Calculator.clearButtonClass;
        },
        _getResultButtonClassName: function() {
            return Calculator.resultButtonClass;
        },
        _getDisplayValue: function() {
            return Calculator.inputDisplayField.value;
        },
        /**
         * _clearInputDisplayErrorMessage
         * @description remove any error message on input display
         * @private
         */
        _clearInputDisplayErrorMessage: function() {
            Calculator.inputDisplayField.value.replace(/[error]+/g, '');
        },
        _clearInputDisplay: function() {
            Calculator.inputDisplayField.value = '';
        },
        /**
         * @method _updateInputDisplayValue
         * @description update display field
         * @private
         * @param  {string} value
         */
        _updateInputDisplayValue: function(value) {
            Calculator.inputDisplayField.value += value;
        },
        /**
         * @method _updateResultDisplayValue
         * @description display evaluated result
         * @private
         * @param  {string} value
         */
        _updateResultDisplayValue: function(value) {
            Calculator.inputDisplayField.value = value;
        },
        _getformattedExpression: function(value) {
            value = Calculator._getMultipleSignIntoStarFormat(value);
            value = Calculator._getDivisionSignIntoForwardSlashFormat(value);
            value = Calculator._getExponentialIntoMathFormat(value);

            return value;
        },
        _getMultipleSignIntoStarFormat: function(value) {
            return value.replace(/[×]+/g, '*');
        },
        _getDivisionSignIntoForwardSlashFormat: function(value) {
            return value.replace(/[÷]+/g, '/');
        },
        _getExponentialIntoMathFormat: function(value) {
            return Calculator.exponentialToMathConverter.getMathFormGivenString(value);
        },
        _evaluateExpression: function(value) {
            try {
                var result = eval(value);
                if ((typeof result === 'number') && result !== 'NaN') {
                    return result;
                } else {
                    return 'error';
                }
            } catch (error) {
                return 'error';
            }
        }
    };


    // Initialize calculator app
    Calculator.init({
        inputDisplayFieldClass: 'calculator-input-display-field',
        numberInputButtonClass: ' number-input',
        operatorInputButtonClass: ' operator-input',
        clearButtonClass: 'button-clear',
        resultButtonClass: 'button-result',
        //instantiate ExponentialToMathConverter class
        exponentialToMathConverter: new ExponentialToMathConverter()
    });
})();
